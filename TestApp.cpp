#include "stdafx.h"
#include <Windows.h>
#include "ROCCAT_Talk.h"
#include <iostream>

HDC hdScreen = GetDC(0);

int redcolor;
int greencolor;
int bluecolor;

int main() {
	//init SDK
	CROCCAT_Talk roccat;

	while (true) {
		COLORREF pixelcolor = GetPixel(hdScreen, 5, 5);

		//get color of pixel
		redcolor = GetRValue(pixelcolor);
		greencolor = GetGValue(pixelcolor);
		bluecolor = GetBValue(pixelcolor);

		//print it to console
		printf("-----\nR: %i \nG: %i \nB: %i\n-----\n", redcolor, greencolor, bluecolor);

		//if more red color make RGM LED red color
		if (redcolor > 100 && greencolor < 100 && bluecolor < 100) {
			roccat.Set_LED_RGB(0x01, 0x01, 0x01, 0xFF, 0x00, 0x00);
			printf("Make it red \n");
		}
		else {
			roccat.Set_LED_RGB(0x01, 0x02, 0x03, 0x00, 0xFF, 0x00);
			printf("Make it green \n");
		}

		//Wait 800ms
		Sleep(800);
	}

	printf("Restore \n");
	roccat.RestoreLEDRGB();
}